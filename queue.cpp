//Name: Brandon Mathis
//File Name: queue.cpp
//Date: 17 November, 2014
//Program Description: Function definitions for linked list based Queue

#include <iostream>
using namespace std;

//===============================================================
//Default constructor for Queue class
template <class Type>
Queue <Type>::Queue()
    {
    count = 0;
    front = 0;	//Sets front pointer to Null
    rear = 0;	//Sets rear pointer to Null
    }
//===============================================================
//Adds to the end of the queue
template <class Type>
bool Queue <Type>::enqueue(Type dataIn)
    {
    Node* temp;
    temp = new Node();
    if (temp == 0)  //New node could not be created
        return false;
    temp -> next = 0;	//Sets the new nodes next pointer to Null
    temp -> data = dataIn;
    if(front == 0)	//If there are no nodes in queue
        {
        rear = temp;
        front = rear;
        }
    else	//There are still nodes
        {
        rear -> next = temp;	//The current last node points to the new last node
        rear = temp;	//Rear pointer points to the new node
        }
    count++;
    return true;
    }
//================================================================
//Removes from the front of the queue
template <class Type>
bool Queue <Type>:: dequeue(Type &dataOut)
    {
    if (count < 1 ) //There aren't any nodes in queue
        return false;
    Node* temp;
    temp = front;
    front = front -> next;  //Top is reassigned to second node in list
    if (front == 0)	//If there are no nodes in queue
        rear = 0;
    dataOut = temp -> data;
    delete temp;	//Delete the previous first node
    count--;
    return true;
    }
//=================================================================
//Checks if there are nodes in the queue
template <class Type>
bool Queue <Type>::isEmpty()
    {
    return count == 0;  //Returns true if there are no nodes in queue
    }
//=================================================================
//Destructor for the Queue class
template <class Type>
Queue <Type>::~Queue()
    {
    Type x; //Temporary variable to pass to pop()
    while(dequeue(x));  //Loops until there are no nodes left in stack
    }