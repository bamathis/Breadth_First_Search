# Breadth_First_Search

[Breadth_First_Search](https://gitlab.com/bamathis/Breadth_First_Search) is a pseudo-social network relationship examining program that implements breadth first search using a queue data structure.

## Quick Start

### Program Execution

```
$ ./Main.cpp
```

### File Input

Reads friend links from file **friends.txt** in the current working directory, where each line in the file is the list of friends of an individual (i.e. the first line is the friends of A, the second line is friends of B).
