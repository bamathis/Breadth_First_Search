//Name: Brandon Mathis
//File Name: Main.cpp
//Date: 17 November, 2014
//Program Description: Driver file for the Cop and the Burglar

#include <iostream>
#include<fstream>
#include <stdlib.h>
#include<string.h>
#include "queue.h"
using namespace std;
void createKnowList(char suspect, string friendList[], bool knowList[]);
bool isBurglar(bool copKnowList[], bool knowList[]);

int main (void)
    {
    char letterArray[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int burglarPosition;
    string friendList[26];
    bool copKnowList[26];
    bool knowList[26];
    ifstream inFile;
    inFile.open("friends.txt");
    burglarPosition = -1;
    for (int n = 0; n < 26; n++)
        {
        if( n < 'F' - 65 || n > 'H' - 65 )
            copKnowList[n] = 1;	//Sets all the positions other than F, G, and H to 1
        else
            copKnowList[n] = 0;	//Sets the F, G, and H positions in the array to 0
        getline(inFile,friendList[n]);
        }
    for (int n = 0; n < 26; n++)
        {
		if(n != 2)
			{
			createKnowList(letterArray[n],friendList, knowList);
			if(isBurglar(copKnowList,knowList))	//Checks if the suspect's list matches the cop's list
				{
				burglarPosition = n;	//Sets the current position in loop as the burglar's position
				n = 26;
				}
			}
        }
    if(burglarPosition > -1)	//Checks if the burglar was found
        cout << "The Burglar is, it is " << letterArray[burglarPosition] <<"." << endl;
    else	//The burglar was not found
        cout << "The Burglar was not found" << endl;
    }
//=================================================================
//Creates a list of all the people that would know if the current suspect is the burglar
void createKnowList(char suspect,string friendList[], bool knowList[])
    {
    Queue<char> myQueue;
    char temp;
    int tempPosition;
    int friendListSize;
    int knowListPosition;
    for (int n = 0; n < 26; n++)	//Resets knowList to all 0
            knowList[n] = 0;
    myQueue.enqueue(suspect);	//Adds the suspect to the queue
    while(!myQueue.isEmpty())	//While there are still people in the queue
        {
        myQueue.dequeue(temp);	//Removes a person from the queue
        tempPosition = temp - 65;	//Changes a char to an int to use as index in arrays
        friendListSize = friendList[tempPosition].length();	//Gets the length of the current persons friend list
        knowList[tempPosition] = 1;	//Adds the current person to the know list
        for(int m = 0; m < friendListSize; m++)
            {
            knowListPosition = (friendList[tempPosition])[m] - 65;	//Changes a char to an int to use as index in arrays
            if(knowListPosition >= 0 && knowListPosition <= 25)	//If knowListPosition would equal a letter
                {
                if(knowList[knowListPosition]== 0 && (friendList[tempPosition])[m] != 32 && 
						(friendList[tempPosition])[m] != 13 && (friendList[tempPosition])[m] != 10)
                   {
                    knowList[knowListPosition] = 1; //Adds the current friend to the know list
                    myQueue.enqueue((friendList[tempPosition])[m]);//Adds the current friend to the queue
                    }
                 }
             }
        }
    }
//==================================================================
//Returns whether the current suspect's know list matches the cop's know list
bool isBurglar(bool copKnowList[], bool knowList[])
    {
    for (int n = 0; n < 26; n++)
        {
        if(copKnowList[n] != knowList[n])	//If any position doesn't match
            return false;
        }
    return true;
    }
