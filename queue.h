//Name: Brandon Mathis
//File Name: queue.h
//Date: 17 November, 2014
//Program Description: Header file for linked list based queue

template <class Type>
class Queue
	{
	private:
	struct Node
		{
		Type data;
		Node* next;
		};
	Node* front;
	Node* rear;
	int count;

	public:
	Queue();
	~Queue();
	bool enqueue(Type dataIn);
	bool dequeue(Type &dataOut);
	bool isEmpty();
	};
#include "queue.cpp"